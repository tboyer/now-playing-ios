//
//  PlayingNowTests.swift
//  PlayingNowTests
//
//  Created by Trevor Boyer on 12/10/14.
//  Copyright (c) 2014 Elite Developer Solutions LLC. All rights reserved.
//

import UIKit
import XCTest

class PlayingNowTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetAlbumArtwork() {

        let song: Song = Song(track: "Cedarwood Road", artist: "U2", album: "Songs of Innocence")

        AlbumLookup.getAlbumArtworkForSong(song,
            success: { (response) in
                XCTAssertEqual(response, "http://a2.mzstatic.com/us/r30/Music3/v4/c0/ac/8f/c0ac8f07-e4f0-9f21-5c32-ce54e2a9cf91/UMG_cvrart_00602547073730_01_RGB72_1500x1500_14UMGIM39480.100x100-75.jpg")
            }, failure: { (statusCode, response) -> () in })
    }
    
}
