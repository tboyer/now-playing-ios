Sample application that pulls the current track metadata from the main Music player. It pulls track, album, artist, and 
album art from the player, but also taps into the
[iTunes Search API](https://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html)
to attempt to pull available album artwork for tracks that didn't have any.

![MainActivity](https://bytebucket.org/tboyer/now-playing-ios/raw/184bf0ee348932b85827d918a8f6c73f1eda80ac/MainActivity.PNG)

## Setup

Clone the project, build, and run! After the app launches, open the music player and play a few
songs. Go back to the app to see your recent plays.