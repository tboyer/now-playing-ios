//
//  RestKitLogger.h
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestKitLogger : NSObject

+(void) initLogging;

@end
