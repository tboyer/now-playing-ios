//
//  ViewController.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 12/10/14.
//  Copyright (c) 2014 Elite Developer Solutions LLC. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer

class ViewController: UITableViewController {
    
    var myPlayer: MPMusicPlayerController?
    var nowPlayingItem: MPMediaItem?
    var notificationCenter: NSNotificationCenter
    var nowPlayingCenter: MPNowPlayingInfoCenter
    var track: String = "Unknown"
    var artist: String = "Unknown"
    var album: String = "Unknown"
    var recentlyPlayed: Array<Song>

    required init(coder aDecoder: NSCoder) {
        self.notificationCenter = NSNotificationCenter.defaultCenter()
        self.nowPlayingCenter = MPNowPlayingInfoCenter.defaultCenter()
        
        // TODO: This should be populated with a database or json file
        recentlyPlayed = Array()

        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        self.notificationCenter = NSNotificationCenter.defaultCenter()
        self.nowPlayingCenter = MPNowPlayingInfoCenter.defaultCenter()
        
        recentlyPlayed = Array()
        
        super.init(nibName: nil, bundle: nil)
    }

    convenience override init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        println("View loaded")
        let info = self.nowPlayingCenter.nowPlayingInfo
        println(info)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents();
        
        // creating simple audio player
        self.myPlayer = MPMusicPlayerController.systemMusicPlayer()
        
        // assing a playback queue containing all media items on the device
        self.myPlayer!.setQueueWithQuery(MPMediaQuery.songsQuery())
        
        self.notificationCenter.addObserver(self,
            selector:"getTrackDescription:",
            name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification,
            object:nil)
        
        self.notificationCenter.addObserver(self,
            selector:"handlePlayBackNotification:",
            name:MPMusicPlayerControllerPlaybackStateDidChangeNotification,
            object:nil)
        
        self.myPlayer!.beginGeneratingPlaybackNotifications()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        UIApplication.sharedApplication().endReceivingRemoteControlEvents();
        
        // TODO: If tracking should continue in the background, this won't be implemented here
        self.notificationCenter.removeObserver(self,
            name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification,
            object:self.myPlayer)
        
        self.notificationCenter.removeObserver(self,
            name:MPMusicPlayerControllerPlaybackStateDidChangeNotification,
            object:self.myPlayer)
        
        self.myPlayer!.endGeneratingPlaybackNotifications()
    }
    
    func handlePlayBackNotification(notification: NSNotification) {
        println(self.myPlayer!.playbackState.rawValue)
        switch (self.myPlayer!.playbackState) {
            case .Playing:
                println("Track is playing")
                break;
            case .Paused:
                println("Track is paused")
                break;
            default:
                break;
        }
    }

    // TODO: Currently doesn't work when playing iTunes Radio
    func getTrackDescription(notification: NSNotification) {
        // Get what's currently playing
        self.nowPlayingItem = self.myPlayer!.nowPlayingItem;

        // Song title currently playing
        self.track = self.nowPlayingItem!.title
        
        // If title is not found "Unknown" will be displayed
        track = track ?? "Unknown"
        
        // Artist currently playing
        self.artist = self.nowPlayingItem!.artist
        
        // If artist is not found "Unknown" will be displayed
        artist = artist ?? "Unknown"
        
        // Current album title
        self.album = self.nowPlayingItem!.albumTitle
        
        // If album title is not found "Unknown" will be displayed
        album = album ?? "Unknown"
        
        println("Track changed - \(artist):\(album):\(track)")
        
        let song: Song = Song(track: track, artist: artist, album: album)
        
        if recentlyPlayed.count == 0 {
            self.addSongToList(song)
        } else {
            var updateList: Bool = true
            
            // If the song hasn't already been added, add it to recentlyPlayed
            for recent in recentlyPlayed {
                if recent.track == track && recent.artist == artist {
                    updateList = false
                }
            }
            
            if updateList {
                self.addSongToList(song)
            }
        }
    }
    
    func addSongToList(song: Song) {
        // Resize our image to fit in the tableview cell
        let artwork: MPMediaItemArtwork? = self.nowPlayingItem!.artwork

        // Default photo
        song.albumArtwork = UIImage(named: "NoMedia")

        if artwork != nil {
            let cropRegion: CGRect = CGRectMake(0, 0, 75, 75)

            // Add it to our object
            song.albumArtwork = artwork!.imageWithSize(cropRegion.size)
        } else {
            // Attempt to pull it from iTunes
            AlbumLookup.getAlbumArtworkForSong(song,
                success: { (response) in
                    println(response)

                    if response != "" {
                        let url: NSURL = NSURL(string: response)!

                        // Pull it from its URL and add it to our object
                        ExternalImage.downloadImage(url,
                            success: { image in
                                song.albumArtwork = image

                                // Update the table
                                self.tableView.reloadData()
                            })
                    }
                }, failure: { (statusCode, response) -> () in
                    println(response)
                    // We couldn't find one - keep the default photo
                })
        }
        
        // Add it to the list
        recentlyPlayed.append(song)
        
        // Update the table
        self.tableView.reloadData()
    }
    
    // MARK: TableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentlyPlayed.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: nil)
        
        let song: Song = recentlyPlayed[indexPath.row]
        
        cell.imageView?.image = song.albumArtwork!
        cell.textLabel?.text = song.track
        cell.detailTextLabel?.text = song.artist
        
        return cell
    }
    
    // MARK: TableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

