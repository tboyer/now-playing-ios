//
//  AlbumLookup.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import Foundation

class AlbumLookup {
    class func getAlbumArtworkForSong(song: Song, success: (response: String) -> (),
        failure: (statusCode: Int, response: String) -> ()) {

        ItunesApi.getAlbumInfoForSong(song,
            success: { (response: Array<ItunesResult>) in
                success(response: response[0].artworkUrl!)
            }, failure: { (code: Int, error: NSError!) in
                failure(statusCode: code, response: error.description)
            })
    }
}