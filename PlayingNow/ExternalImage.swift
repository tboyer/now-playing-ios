//
//  ExternalImage.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import Foundation

class ExternalImage {
    class func getDataFromUrl(urL:NSURL, completion: ((data: NSData?) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(urL) { (data, response, error) in
            completion(data: NSData(data: data))
        }.resume()
    }
    
    class func downloadImage(url:NSURL, success: (image: UIImage?) -> ()) {
        getDataFromUrl(url) { data in
            dispatch_async(dispatch_get_main_queue()) {
                success(image: UIImage(data: data!)!)
            }
        }
    }
}