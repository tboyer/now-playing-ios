//
//  Song.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/6/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import UIKit

class Song {
    var track: String
    var artist: String
    var album: String
    var albumArtwork: UIImage?
    
    init(track: String, artist: String, album: String) {
        self.track = track
        self.artist = artist
        self.album = album
    }
}