//
//  ItunesResult.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import Foundation

class ItunesResult: NSObject {
    var artistId: NSNumber?
    var collectionId: NSNumber?
    var trackId: NSNumber?
    var artistName: String?
    var collectionName: String? // The album name
    var trackName: String?
    var collectionCensoredName: String?
    var trackCensoredName: String?
    var artworkUrl: String? // 100x100 pixels
}