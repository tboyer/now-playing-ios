//
//  RestKitLogger.m
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

#import "RestKitLogger.h"
#import <RestKit/RestKit.h>

@implementation RestKitLogger

+(void) initLogging {
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDebug);
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelTrace);
}

@end
