//
//  ItunesApi.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import Foundation

class ItunesApi {
    
    class func getAlbumInfoForSong(song: Song, success: (response: Array<ItunesResult>) -> (),
        failure: (statusCode: Int, error: NSError!) -> ()) {
            
        let queryParams: Dictionary = [
            "term" : song.album,
            "media" : "music"
        ]

        let resultMapping: RKObjectMapping = RKObjectMapping(forClass: ItunesResult.self);

        // Start implementation
        var manager = RKObjectManager(baseURL: NSURL(string: "https://itunes.apple.com"))

        // Apple returns a JSON string with a content type text/javascript? Why??
//        manager.requestSerializationMIMEType = RKMIMETypeJSON

        // Override the serialization for Javascript to JSON
        RKMIMETypeSerialization.registerClass(RKNSJSONSerialization.self, forMIMEType:"text/javascript")

        let mapping: NSDictionary = [
            "artistId" : "artistId",
            "collectionId" : "collectionId",
            "trackId" : "trackId",
            "artistName" : "artistName",
            "collectionName" : "collectionName",
            "trackName" : "trackName",
            "collectionCensoredName" : "collectionCensoredName",
            "trackCensoredName" : "trackCensoredName",
            "artworkUrl100" : "artworkUrl"
        ]

        // Add the mapping to the manager
        resultMapping.addAttributeMappingsFromDictionary(mapping);

        let responseDescriptor = RKResponseDescriptor(mapping: resultMapping,
            method: RKRequestMethod.GET,
            pathPattern: "/search",
            keyPath: "results", // Grab the 'results' JSON array
            statusCodes: NSIndexSet(index: 200)) // RKStatusCodeClassSuccessful

        manager.addResponseDescriptor(responseDescriptor)

        manager.getObjectsAtPath("/search", parameters: queryParams,
            success: { (operation: RKObjectRequestOperation!, mappingResult: RKMappingResult!) in
                success(response: mappingResult.array() as Array<ItunesResult>)
            }, failure: {(operation: RKObjectRequestOperation!, error: NSError!) in
                failure(statusCode: error.code, error: error!)
            })

    }
}