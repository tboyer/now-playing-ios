//
//  ItunesResponse.swift
//  PlayingNow
//
//  Created by Trevor Boyer on 1/7/15.
//  Copyright (c) 2015 Elite Developer Solutions LLC. All rights reserved.
//

import Foundation

class ItunesResponse: NSObject {
    var resultCount: NSNumber?
    var results: Array<ItunesResult>?
}